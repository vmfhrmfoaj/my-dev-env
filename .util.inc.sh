#!/bin/bash

function find_dev_image {
  local dir=${1:-$(pwd)}
  if [ -f "${dir}/project.clj" ] || [ -f "${dir}/build.boot" ] || [ -f "${dir}/deps.edn" ]; then
    echo 'my-dev-env--clojure'
  elif [ -f  "${dir}/mix.exs" ]; then
    echo 'my-dev-env--elixir'
  else
    echo ''
  fi
}
